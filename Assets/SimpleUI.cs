﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SimpleUI : MonoBehaviour {

    public Text[] texts;
    public Slider[] sliders;

    Button[] buttons;
    private void Awake() {
        buttons = FindObjectsOfType<Button>();
        for (int i = 0; i < buttons.Length; i++) {
            buttons[i].onClick.AddListener(() => {
                TestButtons();
            });
        }
    }
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void TestButtons() {
        float val = Random.Range(0, 99999);
        for (int i = 0; i < texts.Length; i++) {

            texts[i].text = val.ToString();
        }
        for (int i = 0; i < sliders.Length; i++) {
            sliders[i].value = val / 100000;
        }
    }
}
