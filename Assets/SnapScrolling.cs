﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapScrolling : MonoBehaviour {

    public GameObject[] panels;
    [Range(0, 20f)]
    public float snapSpeed;
    [Range(0f, 5f)]
    public float scaleOffset;
    public float space;

    private Vector2[] panPos;
    private Vector2[] panScale;
    private RectTransform contentRect;
    private Vector2 contentVector;

    private int selectedPanel_Index;
    private bool isScrolling;

    void Start() {
        contentRect = GetComponent<RectTransform>();
        panPos = new Vector2[panels.Length];
        panScale = new Vector2[panels.Length];
        for (int i = 0; i < panels.Length; i++) {
            panPos[i] = -panels[i].transform.localPosition;
            //    panScale[i] = panels[i].transform.localScale;
        }
    }

    private void FixedUpdate() {
        float nearestPos = float.MaxValue;
        for (int i = 0; i < panels.Length; i++) {
            float distance = Mathf.Abs(contentRect.anchoredPosition.x - panPos[i].x);
            if (distance < nearestPos) {
                nearestPos = distance;
                selectedPanel_Index = i;
            }
            float scale = Mathf.Clamp(1 / (distance / space) * scaleOffset, 0.5f, 1f);
            panScale[i].x = Mathf.SmoothStep(panels[i].transform.localScale.x, scale, 6 * Time.fixedDeltaTime);
            panScale[i].y = Mathf.SmoothStep(panels[i].transform.localScale.y, scale, 6 * Time.fixedDeltaTime);
            panels[i].transform.localScale = panScale[i];
        }
        if (isScrolling)
            return;
        contentVector.x = Mathf.SmoothStep
            (contentRect.anchoredPosition.x
            , panPos[selectedPanel_Index].x,
            snapSpeed * Time.fixedDeltaTime);
        contentRect.anchoredPosition = contentVector;
    }

    public void Scrolling(bool scroll) {
        isScrolling = scroll;
    }

    public void Click(int i) {
        selectedPanel_Index = i;
        Debug.Log(i);
    }
}
